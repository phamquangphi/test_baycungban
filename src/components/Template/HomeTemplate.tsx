import Footer from "./UI/Footer";
import Header from "./UI/Header";
import Sections from "./UI/Sections";

const HomeTemplate = () => {
  return (
    <div className="max-w-[94.5rem] m-auto sm ">
      <Header />
      <Sections />
      <Footer />
    </div>
  );
};

export default HomeTemplate;
