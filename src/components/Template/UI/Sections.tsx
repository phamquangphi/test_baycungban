import { ArrowRightOutlined, CaretDownOutlined } from "@ant-design/icons";
import { DatePicker, Select } from "antd";
import { useNavigate } from "react-router-dom";
import styled from "styled-components";
import { PATH } from "../../../constant/config";
import moment from "moment";
const Sections = () => {
  const navigate = useNavigate();
  const checkdDate = (date) => {
    const today = moment().startOf("day");
    const futureDate = moment().endOf("day").year(2024);
    return date > today || date > futureDate;
  };
  return (
    <div>
      <SectionS>
        <img
          className=" max-w-[94.5rem] relative top-0 "
          src="/public/images/background.png"
          alt=""
        />
        <p className="absolute top-[35%] left-[12%] text-7xl font-light not-italic leading-[5rem] ">
          Hello! <br />
          Where are <br />
          you <span className="!text-[#4D46FA] !font-medium ">flying</span> to
          ...
        </p>

        <div className=" Ranctangle absolute bottom-[-22%] left-[12%] ">
          <div className="mt-[1.875rem] ml-[1.875rem] mr-[1.875rem]">
            {/* Header */}
            <div className=" flex items-center">
              <div className="flex items-center">
                <p className="w-6 bg-[#F4F2F9] rounded-[100%] text-[#F4F2F9] mr-2">
                  a
                </p>
                <p className="title">One way / Round-trip</p>
              </div>
              <div className="flex items-center pl-[1.875rem]">
                <p className="w-6 bg-[#4D46FA] rounded-[100%] text-[#4D46FA] mr-2">
                  a
                </p>
                <p className="title">Multi-city</p>
              </div>
              <div className="flex items-center pl-[3.125rem]">
                <p className="text-sm font-semibold">
                  <span className="text-[#4D46FA]">02</span> Adult,{" "}
                  <span className="text-[#4D46FA]">01</span> children
                </p>

                <img
                  className="w-2 h-1 ml-1"
                  src="/public/images/Vector 1.png"
                  alt=""
                />
              </div>
              <div className="flex items-center pl-[3.125rem]">
                <Select
                  suffixIcon={
                    <CaretDownOutlined style={{ color: "#4D46FA" }} />
                  }
                  defaultValue="Econonmy Class"
                  style={{ width: 150, fontWeight: "blod", fontSize: "14px" }}
                  bordered={false}
                  options={[
                    { value: "Business Class", label: "Business Class" },
                    { value: "Econonmy Class ", label: "Econonmy Class" },
                  ]}
                />
              </div>
            </div>

            {/* sections */}
            <div className="Option mt-[1.313rem] flex items-center">
              {/* item 1 */}
              <div className="w-[18.75rem] rounded-xl Item-option">
                <div className="title-item ">
                  <p className="text-[rgba(0,0,0,0.5)] text-xs font-semibold tracking-wider uppercase">
                    FROM
                  </p>
                  <h2 className="text-[#4D46FA] text-2xl font-semibold">
                    Da Nang
                  </h2>
                  <p className="text-sm font-semibold">Quang Nam, Viet Nam</p>
                </div>
              </div>

              <img
                className="w-6 ml-2 mr-2"
                src="/public/images/Frame 2.png"
                alt=""
              />

              {/* item 2 */}
              <div className="w-[18.75rem] rounded-xl Item-option">
                <div className="title-item ">
                  <p className="text-[rgba(0,0,0,0.5)] text-xs font-semibold tracking-wider uppercase">
                    To
                  </p>
                  <h2 className="text-[#4D46FA] text-2xl font-semibold">
                    Ho Chi Minh
                  </h2>
                  <p className="text-sm font-semibold">Viet Nam</p>
                </div>
              </div>
              {/* item 3 */}
              <div className="Item-option w-[33.125rem] rounded-xl ml-5 flex justify-between">
                <div className="title-item ">
                  <p className="text-[rgba(0,0,0,0.5)] text-xs font-semibold tracking-wider uppercase">
                    Departure
                  </p>
                  <DatePicker
                    disabledDate={checkdDate}
                    format="DD/MM/YYYY"
                    placeholder="DD/MM/YYYY"
                  />
                  <div className=" flex items-center mt-1">
                    <div className="option-item item01">
                      <p className=" item !text-black">Prev</p>
                    </div>
                    <div className="option-item ml-5">
                      <p className="item">Next</p>
                    </div>
                  </div>
                </div>
                <div className="title-item mr-10 ">
                  <p className="text-[rgba(0,0,0,0.5)] text-xs font-semibold tracking-wider uppercase">
                    return
                  </p>
                  <DatePicker
                    disabledDate={checkdDate}
                    format="DD/MM/YYYY"
                    placeholder="DD/MM/YYYY"
                  />
                  <div className=" flex items-center mt-1">
                    <div className="option-item">
                      <p className=" item">Prev</p>
                    </div>
                    <div className="option-item ml-5">
                      <p className="item">Next</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <button
          onClick={() => {
            navigate(PATH.list);
          }}
          className="absolute right-[10%] bottom-[-27%] w-[15.313rem] h-[3.75rem] text-white text-lg font-semibold bg-[#4D46FA] rounded-xl z-[999]"
        >
          Search Flights <ArrowRightOutlined className="ml-16" />
        </button>
      </SectionS>
    </div>
  );
};

export default Sections;
const SectionS = styled.section`
  .Ranctangle {
    background: #fff;
    box-shadow: 0px 4px 30px 0px rgba(77, 70, 250, 0.1);
    width: 76.875rem;
    height: 14rem;
    border-radius: 0.75rem;
    z-index: 999;
    .title {
      color: #000;
      font-size: 0.938rem;
      font-style: normal;
      font-weight: 600;
      line-height: normal;
    }
    .Option {
      .Item-option {
        border: 1px solid rgba(0, 0, 0, 0.1);
        .title-item {
          padding: 15px 20px;
          .option-item {
            position: relative;
            .item {
              font-size: 0.875rem;
              font-weight: 600;
              color: rgba(0, 0, 0, 0.4);
            }
            .item:hover {
              color: #000;
            }
          }
          .option-item::before {
            content: "";
            background: #000;
            height: 3px;
            width: 0%;
            position: absolute;
            bottom: 0;
            left: 0;
            transition: 0.3s;
          }
          .option-item:hover::before,
          .item01::before {
            width: 100%;
          }
        }
      }
    }
  }
`;
