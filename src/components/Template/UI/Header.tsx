import { NavLink } from "react-router-dom";
import styled from "styled-components";

import { PATH } from "../../../constant/config";

const Header = () => {
  return (
    <HeaderS className=" fixed top-0 w-full sm:w-full">
      <div className="flex items-center">
        <p className="text-2xl font-semibold text-black">BayCungBan</p>
        <img src="/public/images/vietnam.png" className="pr-3 pl-3" alt="" />
        <img src="/public/images/usa.png" alt="" />
      </div>
      <div className="title">
        <NavLink className="Nav-title" to={"/"}>
          Promotion
        </NavLink>
        <NavLink className="Nav-title" to={PATH.list}>
          Flight Schedule
        </NavLink>
        <NavLink className="Nav-title" to={"/"}>
          About us
        </NavLink>
        <NavLink className="Nav-title" to={"/"}>
          Payment Guide
        </NavLink>
      </div>
      <button className="px-4 py-[0.625rem] bg-[#4D46FA] rounded-xl text-white hover:bg-white hover:text-[#4D46FA]">
        Booking now
      </button>
    </HeaderS>
  );
};

export default Header;
const HeaderS = styled.header`
  margin-top: 3rem;
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  z-index: 999;
  .title {
    .Nav-title {
      padding-right: 2rem;
      color: #000;
      text-align: center;
      font-size: 14px;
      font-style: normal;
      font-weight: 500;
      line-height: normal;
      letter-spacing: 0.7px;
    }
    .Nav-title:hover {
      color: white;
      padding: 0.75rem 1rem;
      background: #ffffff1a;
      border-radius: 6.25rem;
    }
  }
`;
