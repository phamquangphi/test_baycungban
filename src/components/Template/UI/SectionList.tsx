import { CaretDownOutlined } from "@ant-design/icons";
import { Select } from "antd";
import styled from "styled-components";

const SectionList = () => {
  const filterOption = (
    input: string,
    option: { label: string; value: string }
  ) => (option?.label ?? "").toLowerCase().includes(input.toLowerCase());
  return (
    <Section className="max-w-[73.125rem] mx-auto pt-4 flex justify-end">
      {/* schedule list */}
      <div className="min-w-[890px]">
        <div className="flex items-center justify-end">
          <p className="text-xs font-semibold tracking-wider text-[rgba(0,0,0,0.5)] mr-3">
            Filter
          </p>
          <div>
            <Select
              suffixIcon={<CaretDownOutlined style={{ color: "#4D46FA" }} />}
              filterOption={filterOption}
              placeholder="Transit"
              style={{
                width: 120,
                background: "#FFF",
                borderRadius: 12,
                marginRight: "5px",
              }}
              bordered={false}
              options={[
                { value: "1", label: "10kg" },
                { value: "2", label: "20kg" },
                { value: "3", label: "30kg" },
              ]}
            />

            <Select
              suffixIcon={<CaretDownOutlined style={{ color: "#4D46FA" }} />}
              filterOption={filterOption}
              placeholder="Time"
              style={{
                width: 120,
                background: "#FFF",
                borderRadius: 12,
                marginRight: "5px",
              }}
              bordered={false}
              options={[
                { value: "1", label: "6AM" },
                { value: "2", label: "12AM" },
                { value: "3", label: "14PM" },
                { value: "4", label: "15PM" },
              ]}
            />
            <Select
              suffixIcon={<CaretDownOutlined style={{ color: "#4D46FA" }} />}
              filterOption={filterOption}
              placeholder="Airline"
              style={{
                width: 120,
                background: "#FFF",
                borderRadius: 12,
                marginRight: "5px",
              }}
              bordered={false}
              options={[
                { value: "1", label: "Bamboo Airways" },
                { value: "2", label: "Vietnam Airlines" },
              ]}
            />
            <Select
              suffixIcon={<CaretDownOutlined style={{ color: "#4D46FA" }} />}
              filterOption={filterOption}
              placeholder="Low Price"
              style={{ width: 120, background: "#FFF", borderRadius: 12 }}
              bordered={false}
              options={[
                { value: "1", label: "100$" },
                { value: "2", label: "200$" },
                { value: "3", label: "300$" },
                { value: "4", label: "400$" },
              ]}
            />
          </div>
        </div>
        <div className="w-[55.625rem]  bg-[#FFFFFF] mt-2 rounded-xl">
          <div className="pt-4 pb-7 pl-4 pr-5">
            {/* header */}
            <div className="flex justify-around">
              {/* Airline */}
              <div className="Airline flex items-center">
                <img src="/public/images/Rectangle 51.png" alt="" />
                <p className="text-sm font-semibold pl-3">BAMBOO AIRWAYS</p>
              </div>
              {/* time */}
              <div className="flex">
                <div className="Time">
                  <p className="text-sm font-semibold tracking-widest pb-[7px]">
                    21:40
                  </p>
                  <p className="px-[0.313rem] py-[0.188] bg-[rgba(0,0,0,0.1)] rounded-[100px] text-[10px] font-semibold text-center">
                    DAD
                  </p>
                </div>
                <div className="time px-[16px] ">
                  <p className="text-sm font-normal text-center">1h 30m</p>
                  <img src="/public/images/Group 9.png" alt="" />
                  <p className="text-center text-xs font-normal">Direct</p>
                </div>
                <div className="Time">
                  <p className="text-sm font-semibold tracking-widest pb-[7px]">
                    21:40
                  </p>
                  <p className="px-[0.313rem] py-[0.188] bg-[rgba(0,0,0,0.1)] rounded-[100px] text-[10px] font-semibold text-center">
                    DAD
                  </p>
                </div>
              </div>
              {/* Transit */}
              <div>
                <div className="flex items-center">
                  <img
                    className="w-[0.875rem] h-[0.875rem]"
                    src="/public/images/suitcase-2 1.png"
                    alt=""
                  />
                  <p className="text-sm font-normal pl-[7px]">
                    Baggage{" "}
                    <span className="text-[#4D46FA] text-sm font-semibold">
                      20kg
                    </span>
                  </p>
                </div>
                <div className="flex items-center">
                  <img
                    className=""
                    src="/public/images/cutlery-2 1.png"
                    alt=""
                  />
                  <p className="text-sm font-normal pl-[7px]">
                    In-flight
                    <span className="text-[#4D46FA] text-sm font-semibold">
                      Meal
                    </span>
                  </p>
                </div>
              </div>
              {/* price */}
              <div>
                <p className="text-sm font-normal text-[rgba(0,0,0,0.5)] line-through ">
                  1,326,000 vnd
                </p>
                <p className="text-[#F06336] text-sm font-semibold">
                  1,322,000 vnd
                </p>
              </div>
              <button className="px-[0.938rem] py-[0.375rem] rounded-xl bg-[rgba(240,99,54,0.2)] text-[#F06336] text-sm font-semibold ">
                Choose
              </button>
            </div>
            {/* footer */}
            <div className="Footer pt-[2.125rem]">
              <div className="flex items-center justify-between">
                <div className="relative before:absolute before:bg-[#4D46FA] before:bottom-0 before:left-0 before:w-[100%] before:h-[1px] before:transition-[0.5s] hover:before:w-[100%]">
                  <p className="text-[#4D46FA] text-xs font-semibold">
                    FLIGHT DETAIL
                  </p>
                </div>
                <div className="relative before:absolute before:bg-[#4D46FA]  before:bottom-0 before:left-0 before:w-[0%] before:h-[1px] before:transition-[0.5s] hover:before:w-[100%]">
                  <p className="text-[rgba(0,0,0,0.4)] text-xs font-semibold hover:text-[#4D46FA]">
                    FARE INFO
                  </p>
                </div>
                <div className="bg-[rgba(0,0,0,0.1)] w-[40.875rem] h-[1px]"></div>
              </div>
              <div className="Detailed-Route pt-[1.188rem] flex justify-between">
                <div className="time">
                  <p className="text-sm font-semibold tracking-widest">21:40</p>
                  <p className="text-xs font-normal tracking-widest ">11Feb</p>
                  <p className="pt-8 pb-[3.188rem] text-sm font-normal">
                    1h 30m
                  </p>
                  <p className="text-sm font-semibold tracking-widest">21:40</p>
                  <p className="text-xs font-normal tracking-widest ">11Feb</p>
                </div>
                <img src="/public/images/Group 5.png" alt="" />
                <div className="location">
                  <div>
                    <p className="text-sm font-semibold">Da nang(DAD)</p>
                    <p className="text-xs font-normal">Da Nang Airport</p>
                  </div>
                  <div className="pt-[6.5rem]">
                    <p className="text-sm font-semibold">
                      Ho Chi Minh City (SGN)
                    </p>
                    <p className="text-xs font-normal">Tansonnhat Intl</p>
                  </div>
                </div>
                <div className="inFo">
                  <div className="Airline flex items-center">
                    <img src="/public/images/Rectangle 51.png" alt="" />
                    <div className="pl-3">
                      <p className="text-sm font-semibold">BAMBOO AIRWAYS</p>
                      <p className="text-xs font-normal">QH-183 . Economy</p>
                    </div>
                  </div>
                  <div className="bg-[#F4F2F9] w-[31.25rem] h-[6.313rem] rounded-xl mt-[10px]">
                    <div className="py-[0.938rem] pl-[0.938rem] pr-[3.938rem] flex justify-around ">
                      <div>
                        <p className="text-sm font-normal pb-1">
                          Baggage{" "}
                          <span className="!font-semibold text-[#4D46FA]">
                            20kg
                          </span>
                        </p>
                        <p className="text-sm font-normal pb-1">
                          In-flight{" "}
                          <span className="!font-semibold text-[#4D46FA]">
                            Meal
                          </span>
                        </p>
                        <p className="text-sm font-normal">
                          In-flight{" "}
                          <span className="!font-semibold text-[#4D46FA]">
                            Entertainment
                          </span>
                        </p>
                      </div>
                      <div>
                        <p className="text-sm font-normal pb-1">
                          Aircraft{" "}
                          <span className="!font-semibold text-[#4D46FA]">
                            Airbus A321
                          </span>
                        </p>
                        <p className="text-sm font-normal pb-1">
                          Seat layout{" "}
                          <span className="!font-semibold text-[#4D46FA]">
                            3-3
                          </span>
                        </p>
                        <p className="text-sm font-normal">
                          Seat pitch{" "}
                          <span className="!font-semibold text-[#4D46FA]">
                            29 inches (standard)
                          </span>
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            {/*  */}
          </div>
          {/* tab */}
        </div>
        {/* item 2 */}
        <div className="w-[55.625rem]  bg-[#FFFFFF] mt-2 rounded-xl">
          <div className="pt-4 pb-7 pl-4 pr-5">
            {/* header */}
            <div className="flex justify-around">
              {/* Airline */}
              <div className="Airline flex items-center">
                <img src="/public/images/Rectangle 51.png" alt="" />
                <p className="text-sm font-semibold pl-3">BAMBOO AIRWAYS</p>
              </div>
              {/* time */}
              <div className="flex">
                <div className="Time">
                  <p className="text-sm font-semibold tracking-widest pb-[7px]">
                    21:40
                  </p>
                  <p className="px-[0.313rem] py-[0.188] bg-[rgba(0,0,0,0.1)] rounded-[100px] text-[10px] font-semibold text-center">
                    DAD
                  </p>
                </div>
                <div className="time px-[16px] ">
                  <p className="text-sm font-normal text-center">1h 30m</p>
                  <img src="/public/images/Group 9.png" alt="" />
                  <p className="text-center text-xs font-normal">Direct</p>
                </div>
                <div className="Time">
                  <p className="text-sm font-semibold tracking-widest pb-[7px]">
                    21:40
                  </p>
                  <p className="px-[0.313rem] py-[0.188] bg-[rgba(0,0,0,0.1)] rounded-[100px] text-[10px] font-semibold text-center">
                    DAD
                  </p>
                </div>
              </div>
              {/* Transit */}
              <div>
                <div className="flex items-center">
                  <img
                    className="w-[0.875rem] h-[0.875rem]"
                    src="/public/images/suitcase-2 1.png"
                    alt=""
                  />
                  <p className="text-sm font-normal pl-[7px]">
                    Baggage{" "}
                    <span className="text-[#4D46FA] text-sm font-semibold">
                      20kg
                    </span>
                  </p>
                </div>
                <div className="flex items-center">
                  <img
                    className=""
                    src="/public/images/cutlery-2 1.png"
                    alt=""
                  />
                  <p className="text-sm font-normal pl-[7px]">
                    In-flight
                    <span className="text-[#4D46FA] text-sm font-semibold">
                      Meal
                    </span>
                  </p>
                </div>
              </div>
              {/* price */}
              <div>
                <p className="text-sm font-normal text-[rgba(0,0,0,0.5)] line-through ">
                  1,326,000 vnd
                </p>
                <p className="text-[#F06336] text-sm font-semibold">
                  1,322,000 vnd
                </p>
              </div>
              <button className="px-[0.938rem] py-[0.375rem] rounded-xl bg-[#F06336] text-white hover:bg-[rgba(240,99,54,0.2)] hover:text-[#F06336] text-sm font-semibold ">
                Choose
              </button>
            </div>
            {/* footer */}
            <div className="Footer pt-[2.125rem]">
              <div className="flex items-center justify-between">
                <div className="relative before:absolute before:bg-[#4D46FA] before:bottom-0 before:left-0 before:w-[0%]  before:h-[1px] before:transition-[0.5s] hover:before:w-[100%]">
                  <p className=" text-xs font-semibold text-[rgba(0,0,0,0.4)] hover:text-[#4D46FA]">
                    FLIGHT DETAIL
                  </p>
                </div>
                <div className="relative before:absolute before:bg-[#4D46FA]  before:bottom-0 before:left-0 before:w-[100%] before:h-[1px] before:transition-[0.5s] hover:before:w-[100%]">
                  <p className=" text-[#4D46FA] text-xs font-semibold ">
                    FARE INFO
                  </p>
                </div>
                <div className="bg-[rgba(0,0,0,0.1)] w-[40.875rem] h-[1px]"></div>
              </div>
              <div className="pt-5 flex">
                <div className="Conditions">
                  <h1 className="text-sm font-semibold">CONDITIONS</h1>
                  <div className="Airline flex items-center pt-[15px]">
                    <img src="/public/images/Rectangle 51.png" alt="" />
                    <div className="pl-3">
                      <p className="text-sm font-semibold">BAMBOO AIRWAYS</p>
                      <p className="text-xs font-normal">QH-183 . Economy</p>
                    </div>
                  </div>
                  <div className=" pt-[14px] flex items-center ">
                    <div>
                      <p className="text-sm font-normal">Da Nang</p>
                      <span className="text-xs font-normal text-[#4D46FA]">
                        Economy
                      </span>
                    </div>
                    <img
                      className="px-1"
                      src="/public/images/Group 9.png"
                      alt=""
                    />
                    <p className="text-sm font-normal">Ho Chi Minh City</p>
                  </div>
                  <p className="pt-[15px] text-xs font-normal">
                    Non - Refundable
                  </p>
                </div>
                <div className="price-Details pl-[7.125rem]">
                  <h1 className="text-sm font-semibold">PRICE DETAILS</h1>
                  <div className="flex justify-around items-center pt-[0.625rem]">
                    <div className="title">
                      <p className="text-sm font-normal leading-6">
                        Adult Basic Fare (x1)
                      </p>
                      <p className="text-sm font-normal leading-6">Tax</p>
                      <p className="text-sm font-normal leading-6">
                        Regular Total Price
                      </p>
                      <p className="text-sm font-normal leading-6 text-[#F06336]">
                        Save
                      </p>
                    </div>
                    <div className="Price pl-[10.688rem]">
                      <p className="text-sm font-semibold leading-6">
                        1,326,000 vnd
                      </p>
                      <p className="text-sm font-normal leading-6">included</p>
                      <p className="text-sm font-normal leading-6">
                        1,326,000 vnd
                      </p>
                      <p className="text-sm font-normal leading-6">
                        -4,000 vnd
                      </p>
                    </div>
                  </div>
                  <hr className="w-[25rem]" />
                  <div className="flex justify-between">
                    <p className="text-sm font-normal leading-6">You pay</p>
                    <p className="text-sm font-semibold text-[#F06336] leading-6 ">
                      1,322,000 vnd
                    </p>
                  </div>
                </div>
              </div>
            </div>
            {/*  */}
          </div>
          {/* tab */}
        </div>
        {/* item 3 */}
        <div className="w-[55.625rem]  bg-[#FFFFFF] mt-2 rounded-xl">
          <div className="pt-4 pb-7 pl-4 pr-5">
            {/* header */}
            <div className="flex justify-around">
              {/* Airline */}
              <div className="Airline flex items-center">
                <img src="/public/images/pngegg-2 2.png" alt="" />
                <p className="text-sm font-semibold pl-3">VIETNAM AIRLINES</p>
              </div>
              {/* time */}
              <div className="flex">
                <div className="Time">
                  <p className="text-sm font-semibold tracking-widest pb-[7px]">
                    21:40
                  </p>
                  <p className="px-[0.313rem] py-[0.188] bg-[rgba(0,0,0,0.1)] rounded-[100px] text-[10px] font-semibold text-center">
                    DAD
                  </p>
                </div>
                <div className="time px-[16px] ">
                  <p className="text-sm font-normal text-center">1h 30m</p>
                  <img src="/public/images/Group 9.png" alt="" />
                  <p className="text-center text-xs font-normal">Direct</p>
                </div>
                <div className="Time">
                  <p className="text-sm font-semibold tracking-widest pb-[7px]">
                    21:40
                  </p>
                  <p className="px-[0.313rem] py-[0.188] bg-[rgba(0,0,0,0.1)] rounded-[100px] text-[10px] font-semibold text-center">
                    DAD
                  </p>
                </div>
              </div>
              {/* Transit */}
              <div>
                <div className="flex items-center">
                  <img
                    className="w-[0.875rem] h-[0.875rem]"
                    src="/public/images/suitcase-2 1.png"
                    alt=""
                  />
                  <p className="text-sm font-normal pl-[7px]">
                    Baggage{" "}
                    <span className="text-[#4D46FA] text-sm font-semibold">
                      20kg
                    </span>
                  </p>
                </div>
                <div className="flex items-center">
                  <img
                    className=""
                    src="/public/images/cutlery-2 1.png"
                    alt=""
                  />
                  <p className="text-sm font-normal pl-[7px]">
                    In-flight
                    <span className="text-[#4D46FA] text-sm font-semibold">
                      Meal
                    </span>
                  </p>
                </div>
              </div>
              {/* price */}
              <div>
                <p className="text-sm font-normal text-[rgba(0,0,0,0.5)] line-through ">
                  1,326,000 vnd
                </p>
                <p className="text-[#F06336] text-sm font-semibold">
                  1,322,000 vnd
                </p>
              </div>
              <button className="px-[0.938rem] py-[0.375rem] rounded-xl bg-[rgba(240,99,54,0.2)] text-[#F06336] hover:bg-[#F06336] hover:text-white text-sm font-semibold ">
                Choose
              </button>
            </div>
            {/* footer */}
            <div className="Footer pt-[2.125rem]">
              <div className="flex items-center justify-start">
                <div className="relative before:absolute before:bg-[#4D46FA] before:bottom-0 before:left-0 before:w-[100%] before:h-[1px] before:transition-[0.5s] hover:before:w-[100%]">
                  <p className="text-[#4D46FA] text-xs font-semibold">
                    FLIGHT DETAIL
                  </p>
                </div>
                <div className="relative before:absolute before:bg-[#4D46FA]  before:bottom-0 before:left-0 before:w-[0%] before:h-[1px] before:transition-[0.5s] hover:before:w-[100%] ml-[30px]">
                  <p className="text-[rgba(0,0,0,0.4)] text-xs font-semibold hover:text-[#4D46FA]">
                    FARE INFO
                  </p>
                </div>
              </div>
            </div>
            {/*  */}
          </div>
          {/* tab */}
        </div>
        {/* item 4 */}
        <div className="w-[55.625rem]  bg-[#FFFFFF] mt-2 rounded-xl">
          <div className="pt-4 pb-7 pl-4 pr-5">
            {/* header */}
            <div className="flex justify-around">
              {/* Airline */}
              <div className="Airline flex items-center">
                <img src="/public/images/Rectangle 51.png" alt="" />
                <p className="text-sm font-semibold pl-3">BAMBOO AIRWAYS</p>
              </div>
              {/* time */}
              <div className="flex">
                <div className="Time">
                  <p className="text-sm font-semibold tracking-widest pb-[7px]">
                    21:40
                  </p>
                  <p className="px-[0.313rem] py-[0.188] bg-[rgba(0,0,0,0.1)] rounded-[100px] text-[10px] font-semibold text-center">
                    DAD
                  </p>
                </div>
                <div className="time px-[16px] ">
                  <p className="text-sm font-normal text-center">1h 30m</p>
                  <img src="/public/images/Group 9.png" alt="" />
                  <p className="text-center text-xs font-normal">Direct</p>
                </div>
                <div className="Time">
                  <p className="text-sm font-semibold tracking-widest pb-[7px]">
                    21:40
                  </p>
                  <p className="px-[0.313rem] py-[0.188] bg-[rgba(0,0,0,0.1)] rounded-[100px] text-[10px] font-semibold text-center">
                    DAD
                  </p>
                </div>
              </div>
              {/* Transit */}
              <div>
                <div className="flex items-center">
                  <img
                    className="w-[0.875rem] h-[0.875rem]"
                    src="/public/images/suitcase-2 1.png"
                    alt=""
                  />
                  <p className="text-sm font-normal pl-[7px]">
                    Baggage{" "}
                    <span className="text-[#4D46FA] text-sm font-semibold">
                      20kg
                    </span>
                  </p>
                </div>
                <div className="flex items-center">
                  <img
                    className=""
                    src="/public/images/cutlery-2 1.png"
                    alt=""
                  />
                  <p className="text-sm font-normal pl-[7px]">
                    In-flight
                    <span className="text-[#4D46FA] text-sm font-semibold">
                      Meal
                    </span>
                  </p>
                </div>
              </div>
              {/* price */}
              <div>
                <p className="text-sm font-normal text-[rgba(0,0,0,0.5)] line-through ">
                  1,326,000 vnd
                </p>
                <p className="text-[#F06336] text-sm font-semibold">
                  1,322,000 vnd
                </p>
              </div>
              <button className="px-[0.938rem] py-[0.375rem] rounded-xl hover:bg-[#F06336] hover:text-white bg-[rgba(240,99,54,0.2)] text-[#F06336] text-sm font-semibold ">
                Choose
              </button>
            </div>
            {/* footer */}
            <div className="Footer pt-[2.125rem]">
              <div className="flex items-center">
                <div className="relative before:absolute before:bg-[#4D46FA] before:bottom-0 before:left-0 before:w-[0%]  before:h-[1px] before:transition-[0.5s] hover:before:w-[100%] ">
                  <p className=" text-xs font-semibold text-[rgba(0,0,0,0.4)] hover:text-[#4D46FA]">
                    FLIGHT DETAIL
                  </p>
                </div>
                <div className="relative before:absolute before:bg-[#4D46FA]  before:bottom-0 before:left-0 before:w-[100%] before:h-[1px] before:transition-[0.5s] hover:before:w-[100%] ml-[30px]">
                  <p className=" text-[#4D46FA] text-xs font-semibold ">
                    FARE INFO
                  </p>
                </div>
              </div>
            </div>
            {/*  */}
          </div>
          {/* tab */}
        </div>
        {/* item 5 */}
        <div className="w-[55.625rem]  bg-[#FFFFFF] mt-2 rounded-xl">
          <div className="pt-4 pb-7 pl-4 pr-5">
            {/* header */}
            <div className="flex justify-around">
              {/* Airline */}
              <div className="Airline flex items-center">
                <img src="/public/images/pngegg-2 2.png" alt="" />
                <p className="text-sm font-semibold pl-3">VIETNAM AIRLINES</p>
              </div>
              {/* time */}
              <div className="flex">
                <div className="Time">
                  <p className="text-sm font-semibold tracking-widest pb-[7px]">
                    21:40
                  </p>
                  <p className="px-[0.313rem] py-[0.188] bg-[rgba(0,0,0,0.1)] rounded-[100px] text-[10px] font-semibold text-center">
                    DAD
                  </p>
                </div>
                <div className="time px-[16px] ">
                  <p className="text-sm font-normal text-center">1h 30m</p>
                  <img src="/public/images/Group 9.png" alt="" />
                  <p className="text-center text-xs font-normal">Direct</p>
                </div>
                <div className="Time">
                  <p className="text-sm font-semibold tracking-widest pb-[7px]">
                    21:40
                  </p>
                  <p className="px-[0.313rem] py-[0.188] bg-[rgba(0,0,0,0.1)] rounded-[100px] text-[10px] font-semibold text-center">
                    DAD
                  </p>
                </div>
              </div>
              {/* Transit */}
              <div>
                <div className="flex items-center">
                  <img
                    className="w-[0.875rem] h-[0.875rem]"
                    src="/public/images/suitcase-2 1.png"
                    alt=""
                  />
                  <p className="text-sm font-normal pl-[7px]">
                    Baggage{" "}
                    <span className="text-[#4D46FA] text-sm font-semibold">
                      20kg
                    </span>
                  </p>
                </div>
                <div className="flex items-center">
                  <img
                    className=""
                    src="/public/images/cutlery-2 1.png"
                    alt=""
                  />
                  <p className="text-sm font-normal pl-[7px]">
                    In-flight
                    <span className="text-[#4D46FA] text-sm font-semibold">
                      Meal
                    </span>
                  </p>
                </div>
              </div>
              {/* price */}
              <div>
                <p className="text-sm font-normal text-[rgba(0,0,0,0.5)] line-through ">
                  1,326,000 vnd
                </p>
                <p className="text-[#F06336] text-sm font-semibold">
                  1,322,000 vnd
                </p>
              </div>
              <button className="px-[0.938rem] py-[0.375rem] rounded-xl bg-[rgba(240,99,54,0.2)] text-[#F06336] hover:bg-[#F06336] hover:text-white text-sm font-semibold ">
                Choose
              </button>
            </div>
            {/* footer */}
            <div className="Footer pt-[2.125rem]">
              <div className="flex items-center justify-start">
                <div className="relative before:absolute before:bg-[#4D46FA] before:bottom-0 before:left-0 before:w-[100%] before:h-[1px] before:transition-[0.5s] hover:before:w-[100%]">
                  <p className="text-[#4D46FA] text-xs font-semibold">
                    FLIGHT DETAIL
                  </p>
                </div>
                <div className="relative before:absolute before:bg-[#4D46FA]  before:bottom-0 before:left-0 before:w-[0%] before:h-[1px] before:transition-[0.5s] hover:before:w-[100%] ml-[30px]">
                  <p className="text-[rgba(0,0,0,0.4)] text-xs font-semibold hover:text-[#4D46FA]">
                    FARE INFO
                  </p>
                </div>
              </div>
            </div>
            {/*  */}
          </div>
          {/* tab */}
        </div>
        {/* item 6 */}
        <div className="w-[55.625rem]  bg-[#FFFFFF] mt-2 rounded-xl">
          <div className="pt-4 pb-7 pl-4 pr-5">
            {/* header */}
            <div className="flex justify-around">
              {/* Airline */}
              <div className="Airline flex items-center">
                <img src="/public/images/Rectangle 51.png" alt="" />
                <p className="text-sm font-semibold pl-3">BAMBOO AIRWAYS</p>
              </div>
              {/* time */}
              <div className="flex">
                <div className="Time">
                  <p className="text-sm font-semibold tracking-widest pb-[7px]">
                    21:40
                  </p>
                  <p className="px-[0.313rem] py-[0.188] bg-[rgba(0,0,0,0.1)] rounded-[100px] text-[10px] font-semibold text-center">
                    DAD
                  </p>
                </div>
                <div className="time px-[16px] ">
                  <p className="text-sm font-normal text-center">1h 30m</p>
                  <img src="/public/images/Group 9.png" alt="" />
                  <p className="text-center text-xs font-normal">Direct</p>
                </div>
                <div className="Time">
                  <p className="text-sm font-semibold tracking-widest pb-[7px]">
                    21:40
                  </p>
                  <p className="px-[0.313rem] py-[0.188] bg-[rgba(0,0,0,0.1)] rounded-[100px] text-[10px] font-semibold text-center">
                    DAD
                  </p>
                </div>
              </div>
              {/* Transit */}
              <div>
                <div className="flex items-center">
                  <img
                    className="w-[0.875rem] h-[0.875rem]"
                    src="/public/images/suitcase-2 1.png"
                    alt=""
                  />
                  <p className="text-sm font-normal pl-[7px]">
                    Baggage{" "}
                    <span className="text-[#4D46FA] text-sm font-semibold">
                      20kg
                    </span>
                  </p>
                </div>
                <div className="flex items-center">
                  <img
                    className=""
                    src="/public/images/cutlery-2 1.png"
                    alt=""
                  />
                  <p className="text-sm font-normal pl-[7px]">
                    In-flight
                    <span className="text-[#4D46FA] text-sm font-semibold">
                      Meal
                    </span>
                  </p>
                </div>
              </div>
              {/* price */}
              <div>
                <p className="text-sm font-normal text-[rgba(0,0,0,0.5)] line-through ">
                  1,326,000 vnd
                </p>
                <p className="text-[#F06336] text-sm font-semibold">
                  1,322,000 vnd
                </p>
              </div>
              <button className="px-[0.938rem] py-[0.375rem] rounded-xl hover:bg-[#F06336] hover:text-white bg-[rgba(240,99,54,0.2)] text-[#F06336] text-sm font-semibold ">
                Choose
              </button>
            </div>
            {/* footer */}
            <div className="Footer pt-[2.125rem]">
              <div className="flex items-center">
                <div className="relative before:absolute before:bg-[#4D46FA] before:bottom-0 before:left-0 before:w-[0%]  before:h-[1px] before:transition-[0.5s] hover:before:w-[100%] ">
                  <p className=" text-xs font-semibold text-[rgba(0,0,0,0.4)] hover:text-[#4D46FA]">
                    FLIGHT DETAIL
                  </p>
                </div>
                <div className="relative before:absolute before:bg-[#4D46FA]  before:bottom-0 before:left-0 before:w-[100%] before:h-[1px] before:transition-[0.5s] hover:before:w-[100%] ml-[30px]">
                  <p className=" text-[#4D46FA] text-xs font-semibold ">
                    FARE INFO
                  </p>
                </div>
              </div>
            </div>
            {/*  */}
          </div>
          {/* tab */}
        </div>
        {/* item 7 */}
        <div className="w-[55.625rem]  bg-[#FFFFFF] mt-2 rounded-xl">
          <div className="pt-4 pb-7 pl-4 pr-5">
            {/* header */}
            <div className="flex justify-around">
              {/* Airline */}
              <div className="Airline flex items-center">
                <img src="/public/images/pngegg-2 2.png" alt="" />
                <p className="text-sm font-semibold pl-3">VIETNAM AIRLINES</p>
              </div>
              {/* time */}
              <div className="flex">
                <div className="Time">
                  <p className="text-sm font-semibold tracking-widest pb-[7px]">
                    21:40
                  </p>
                  <p className="px-[0.313rem] py-[0.188] bg-[rgba(0,0,0,0.1)] rounded-[100px] text-[10px] font-semibold text-center">
                    DAD
                  </p>
                </div>
                <div className="time px-[16px] ">
                  <p className="text-sm font-normal text-center">1h 30m</p>
                  <img src="/public/images/Group 9.png" alt="" />
                  <p className="text-center text-xs font-normal">Direct</p>
                </div>
                <div className="Time">
                  <p className="text-sm font-semibold tracking-widest pb-[7px]">
                    21:40
                  </p>
                  <p className="px-[0.313rem] py-[0.188] bg-[rgba(0,0,0,0.1)] rounded-[100px] text-[10px] font-semibold text-center">
                    DAD
                  </p>
                </div>
              </div>
              {/* Transit */}
              <div>
                <div className="flex items-center">
                  <img
                    className="w-[0.875rem] h-[0.875rem]"
                    src="/public/images/suitcase-2 1.png"
                    alt=""
                  />
                  <p className="text-sm font-normal pl-[7px]">
                    Baggage{" "}
                    <span className="text-[#4D46FA] text-sm font-semibold">
                      20kg
                    </span>
                  </p>
                </div>
                <div className="flex items-center">
                  <img
                    className=""
                    src="/public/images/cutlery-2 1.png"
                    alt=""
                  />
                  <p className="text-sm font-normal pl-[7px]">
                    In-flight
                    <span className="text-[#4D46FA] text-sm font-semibold">
                      Meal
                    </span>
                  </p>
                </div>
              </div>
              {/* price */}
              <div>
                <p className="text-sm font-normal text-[rgba(0,0,0,0.5)] line-through ">
                  1,326,000 vnd
                </p>
                <p className="text-[#F06336] text-sm font-semibold">
                  1,322,000 vnd
                </p>
              </div>
              <button className="px-[0.938rem] py-[0.375rem] rounded-xl bg-[rgba(240,99,54,0.2)] text-[#F06336] hover:bg-[#F06336] hover:text-white text-sm font-semibold ">
                Choose
              </button>
            </div>
            {/* footer */}
            <div className="Footer pt-[2.125rem]">
              <div className="flex items-center justify-start">
                <div className="relative before:absolute before:bg-[#4D46FA] before:bottom-0 before:left-0 before:w-[100%] before:h-[1px] before:transition-[0.5s] hover:before:w-[100%]">
                  <p className="text-[#4D46FA] text-xs font-semibold">
                    FLIGHT DETAIL
                  </p>
                </div>
                <div className="relative before:absolute before:bg-[#4D46FA]  before:bottom-0 before:left-0 before:w-[0%] before:h-[1px] before:transition-[0.5s] hover:before:w-[100%] ml-[30px]">
                  <p className="text-[rgba(0,0,0,0.4)] text-xs font-semibold hover:text-[#4D46FA]">
                    FARE INFO
                  </p>
                </div>
              </div>
            </div>
            {/*  */}
          </div>
          {/* tab */}
        </div>
        {/* item 8 */}
        <div className="w-[55.625rem]  bg-[#FFFFFF] mt-2 rounded-xl">
          <div className="pt-4 pb-7 pl-4 pr-5">
            {/* header */}
            <div className="flex justify-around">
              {/* Airline */}
              <div className="Airline flex items-center">
                <img src="/public/images/Rectangle 51.png" alt="" />
                <p className="text-sm font-semibold pl-3">BAMBOO AIRWAYS</p>
              </div>
              {/* time */}
              <div className="flex">
                <div className="Time">
                  <p className="text-sm font-semibold tracking-widest pb-[7px]">
                    21:40
                  </p>
                  <p className="px-[0.313rem] py-[0.188] bg-[rgba(0,0,0,0.1)] rounded-[100px] text-[10px] font-semibold text-center">
                    DAD
                  </p>
                </div>
                <div className="time px-[16px] ">
                  <p className="text-sm font-normal text-center">1h 30m</p>
                  <img src="/public/images/Group 9.png" alt="" />
                  <p className="text-center text-xs font-normal">Direct</p>
                </div>
                <div className="Time">
                  <p className="text-sm font-semibold tracking-widest pb-[7px]">
                    21:40
                  </p>
                  <p className="px-[0.313rem] py-[0.188] bg-[rgba(0,0,0,0.1)] rounded-[100px] text-[10px] font-semibold text-center">
                    DAD
                  </p>
                </div>
              </div>
              {/* Transit */}
              <div>
                <div className="flex items-center">
                  <img
                    className="w-[0.875rem] h-[0.875rem]"
                    src="/public/images/suitcase-2 1.png"
                    alt=""
                  />
                  <p className="text-sm font-normal pl-[7px]">
                    Baggage{" "}
                    <span className="text-[#4D46FA] text-sm font-semibold">
                      20kg
                    </span>
                  </p>
                </div>
                <div className="flex items-center">
                  <img
                    className=""
                    src="/public/images/cutlery-2 1.png"
                    alt=""
                  />
                  <p className="text-sm font-normal pl-[7px]">
                    In-flight
                    <span className="text-[#4D46FA] text-sm font-semibold">
                      Meal
                    </span>
                  </p>
                </div>
              </div>
              {/* price */}
              <div>
                <p className="text-sm font-normal text-[rgba(0,0,0,0.5)] line-through ">
                  1,326,000 vnd
                </p>
                <p className="text-[#F06336] text-sm font-semibold">
                  1,322,000 vnd
                </p>
              </div>
              <button className="px-[0.938rem] py-[0.375rem] rounded-xl hover:bg-[#F06336] hover:text-white bg-[rgba(240,99,54,0.2)] text-[#F06336] text-sm font-semibold ">
                Choose
              </button>
            </div>
            {/* footer */}
            <div className="Footer pt-[2.125rem]">
              <div className="flex items-center">
                <div className="relative before:absolute before:bg-[#4D46FA] before:bottom-0 before:left-0 before:w-[0%]  before:h-[1px] before:transition-[0.5s] hover:before:w-[100%] ">
                  <p className=" text-xs font-semibold text-[rgba(0,0,0,0.4)] hover:text-[#4D46FA]">
                    FLIGHT DETAIL
                  </p>
                </div>
                <div className="relative before:absolute before:bg-[#4D46FA]  before:bottom-0 before:left-0 before:w-[100%] before:h-[1px] before:transition-[0.5s] hover:before:w-[100%] ml-[30px]">
                  <p className=" text-[#4D46FA] text-xs font-semibold ">
                    FARE INFO
                  </p>
                </div>
              </div>
            </div>
            {/*  */}
          </div>
          {/* tab */}
        </div>
        {/* all */}
      </div>
      {/* prepare the bill */}
      <div className="min-w-[16.875rem] ml-[0.625rem]">
        <div className=" bg-white rounded-t-xl">
          <h1 className="text-sm font-semibold tracking-wider px-[0.938rem] py-[0.938rem]">
            YOUR FLIGHTS
          </h1>
          <hr className="w-full" />
          <div className="px-[0.938rem] py-[0.938rem]">
            <div className="flex items-center">
              <div className="w-[1.875rem] h-[1.875rem] bg-[#979797] rounded-[6.25rem] text-white text-sm font-semibold text-center leading-[1.875rem]">
                01
              </div>
              <div className="pl-3">
                <p className="text-sm font-normal">Fri, 11 Feb, 2022</p>
                <p className="text-sm font-semibold">Da Nang - Ho Chi Minh</p>
              </div>
            </div>
            <div className="Airline flex items-center pt-4">
              <img src="/public/images/Rectangle 51.png" alt="" />
              <div className="pl-3">
                <p className="text-sm font-semibold ">BAMBOO AIRWAYS</p>
                <p className="text-xs font-semibold underline text-[#4D46FA]">
                  Details
                </p>
              </div>
            </div>
            <div className="flex pt-[0.938rem]">
              <div className="Time">
                <p className="text-sm font-semibold tracking-widest pb-[7px]">
                  21:40
                </p>
                <p className="px-[0.313rem] py-[0.188] bg-[rgba(0,0,0,0.1)] rounded-[100px] text-[10px] font-semibold text-center">
                  DAD
                </p>
              </div>
              <div className="time px-[16px] ">
                <p className="text-sm font-normal text-center">1h 30m</p>
                <img src="/public/images/Group 9.png" alt="" />
                <p className="text-center text-xs font-normal">Direct</p>
              </div>
              <div className="Time">
                <p className="text-sm font-semibold tracking-widest pb-[7px]">
                  21:40
                </p>
                <p className="px-[0.313rem] py-[0.188] bg-[rgba(0,0,0,0.1)] rounded-[100px] text-[10px] font-semibold text-center">
                  DAD
                </p>
              </div>
            </div>
            <button className="w-60 mt-5 py-[0.625rem] px-4 bg-[rgba(77,70,250,0.1)] rounded-xl text-[#4D46FA] text-xs font-semibold hover:bg-[#4D46FA] hover:text-white">
              Change departure flight
            </button>
            <hr className="w-full mt-[0.938rem]" />
            <div className="flex items-center pt-[0.938rem]">
              <div className="w-[1.875rem] h-[1.875rem] bg-[#4D46FA] rounded-[6.25rem] text-white text-sm font-semibold text-center leading-[1.875rem]">
                02
              </div>
              <div className="pl-3">
                <p className="text-sm font-normal">Sun, 13 Feb, 2022</p>
                <p className="text-sm font-semibold">Ho Chi Minh - Da Nang</p>
              </div>
            </div>
          </div>
        </div>
        <div className="bg-[#F8F8F8] rounded-b-xl">
          <div className="px-[0.938rem] py-[0.938rem]">
            <p className="text-sm font-normal">Subtotal</p>
            <p className="text-sm font-semibold text-[#F06336]">
              1,322,000 vnd
            </p>
          </div>
        </div>
      </div>
    </Section>
  );
};

export default SectionList;
const Section = styled.section``;
