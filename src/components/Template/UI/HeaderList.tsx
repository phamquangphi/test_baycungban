import { SearchOutlined } from "@ant-design/icons";
import { NavLink } from "react-router-dom";
import styled from "styled-components";

const HeaderList = () => {
  return (
    <div>
      <HeaderS className=" bg-[#4D46FA] w-full h-20">
        <div className="flex items-center">
          <p className="text-2xl font-semibold text-white">BayCungBan</p>
          <img src="/public/images/vietnam.png" className="mr-3 ml-3" alt="" />
          <img src="/public/images/usa.png" alt="" />
        </div>
        <div className="title">
          <NavLink className="Nav-title" to={""}>
            Promotion
          </NavLink>
          <NavLink
            className="Nav-title bg-[#FFFFFF1A] px-4 py-3 rounded-[6.25rem]"
            to={""}
          >
            Flight Schedule
          </NavLink>
          <NavLink className="Nav-title" to={""}>
            About us
          </NavLink>
          <NavLink className="Nav-title" to={""}>
            Payment Guide
          </NavLink>
        </div>
        <button className="px-4 py-[0.625rem] bg-white rounded-xl text-[#4D46FA] hover:bg-[#FFFFFF1A] hover:text-white">
          Booking now
        </button>
      </HeaderS>
      <Sections className="bg-[#FFF] w-full h-20 ">
        {/* lịch trình */}
        <div className="flex items-center">
          <div className="ml-8">
            <h2 className="text-[#4D46FA] text-base font-semibold ">
              Da Nang(DAD)
            </h2>
            <p className="text-xs font-normal">Fri, 22 Mar, 2022</p>
          </div>
          <img
            className="w-[1.563rem] h-[0.875rem] mx-[3.125rem]"
            src="/public/images/Frame 4.png"
            alt=""
          />
          <div>
            <h2 className="text-[#4D46FA] text-base font-semibold ">
              Ho Chi Minh (SGN)
            </h2>
            <p className="text-xs font-normal">Fri, 22 Mar, 2022</p>
          </div>
        </div>
        {/* option */}
        <div className="flex items-center ">
          <p className="text-sm font-semibold">Round-trip</p>
          <div className="bg-[rgba(0,0,0,0.1)] h-6 w-[2px] mx-4"></div>
          <p className="text-sm font-semibold">
            <span className="text-[#4D46FA]">02</span> Adult,{" "}
            <span className="text-[#4D46FA]">01</span> children
          </p>
          <div className="bg-[rgba(0,0,0,0.1)] h-6 w-[2px] mx-4"></div>
          <p className="text-sm font-semibold">Business Class</p>
        </div>
        {/* button search */}
        <button className="px-4 py-[0.625rem] bg-[#F06336] rounded-xl text-white mr-8">
          Change Flights{" "}
          <SearchOutlined
            style={{
              fontSize: "0.875rem",
              display: "inline-block",
              marginLeft: "0.313rem",
            }}
          />
        </button>
      </Sections>
    </div>
  );
};

export default HeaderList;
const HeaderS = styled.header`
  display: flex;
  justify-content: space-evenly;
  align-items: center;
  .title {
    .Nav-title {
      margin-right: 2rem;
      color: white;
      text-align: center;
      font-size: 14px;
      font-style: normal;
      font-weight: 500;
      line-height: normal;
      letter-spacing: 0.7px;
    }
    .Nav-title:hover {
      padding: 0.75rem 1rem;
      background: #ffffff1a;
      border-radius: 6.25rem;
    }
  }
`;
const Sections = styled.section`
  display: flex;
  justify-content: space-evenly;
  align-items: center;
`;
