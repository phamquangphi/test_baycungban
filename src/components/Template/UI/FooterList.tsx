const FooterList = () => {
  return (
    <div className="flex justify-between py-[50px] max-w-[73.125rem] mx-auto">
      <div className="item flex items-center">
        <div className="flex items-center">
          <img src="/public/images/phone.png" alt="" className="pr-1  " />
          <p className="text-sm font-normal">Call us: +84 908 02 02 58</p>
        </div>
        <div className="flex items-center ml-8">
          <img src="/public/images/email.png" alt="" className="pr-1" />
          <p className="text-sm font-normal">Email: chucinog@gmail.com</p>
        </div>
      </div>
      <div className="item flex items-center">
        <p className="text-sm font-normal">Follow us</p>
        <p className="h-[2px] w-10 bg-black mx-3"></p>
        <img className="mr-3" src="/public/images/facebook.png" alt="" />
        <img src="/public/images/instagram.png" alt="" />
      </div>
    </div>
  );
};

export default FooterList;
