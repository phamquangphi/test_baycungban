import FooterList from "./UI/FooterList";
import HeaderList from "./UI/HeaderList";
import SectionList from "./UI/SectionList";

const ListTemplate = () => {
  return (
    <div className="max-w-[106.25rem] m-auto">
      <HeaderList />
      <div className=" bg-[#F4F2F9]">
        <SectionList />
        <FooterList />
      </div>
    </div>
  );
};

export default ListTemplate;
