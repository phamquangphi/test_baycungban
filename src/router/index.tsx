import { RouteObject } from "react-router-dom";
import Home from "../pages/Home";

import { PATH } from "../constant/config";
import List from "../pages/List";
export const router: RouteObject[] = [
  {
    path: "/",
    element: <Home />,
  },
  {
    path: PATH.list,
    element: <List />,
  },
];
