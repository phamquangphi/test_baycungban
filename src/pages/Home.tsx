import HomeTemplate from "../components/Template/HomeTemplate";

const Home = () => {
  return (
    <div>
      <HomeTemplate />
    </div>
  );
};

export default Home;
