import ListTemplate from "../components/Template/ListTemplate";

const List = () => {
  return (
    <div>
      <ListTemplate />
    </div>
  );
};

export default List;
